﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Android.App;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ShopNote")]
[assembly: AssemblyDescription("An Android Application for the whole family to record the weekly shop, so nothing gets left or forgotten. ")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Harker Industries")]
[assembly: AssemblyProduct("ShopNote")]
[assembly: AssemblyCopyright("Copyright © Ollie Harker 2018")]
[assembly: AssemblyTrademark("Ollie Harker")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
