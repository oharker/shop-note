# Shop-Note

An Android Application for the whole family to record the weekly shop, so nothing gets left or forgotten.

Key elements are:

* Adding items to be bought

* Categorizing by priority or shop.

* Crossing out items

* Notifiying when items are added

* Adding family members to the group

* Synchronizing when online, so internet access is not crucial.

# Technologies

The application is built using the Xaramin Cross Platform Framework. To publish the software, the relevant Xaramin and Android SDK Tools will be required.
